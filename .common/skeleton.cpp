#include <iostream>

namespace {

void solve(std::size_t tt) {
  std::cout << "Case #" << tt << ": ";

  // TODO
}

}  // namespace

int main(void) {
  std::size_t T;
  std::cin >> T;
  for (auto i = 1u; i <= T; ++i) {
    solve(i);
  }

  return 0;
}
